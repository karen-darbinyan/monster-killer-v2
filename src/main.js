import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import start from "./components/startGame.vue";
import game from "./components/game.vue";
import endGame from "./components/endGame.vue";

Vue.use(VueRouter);
Vue.config.productionTip = false;

const routes = [
  { path: "/", component: start },
  {
    path: "/playing",
    name: "playing",
    component: game,
  },
  {
    path: "/end/:winner/:p1h/:p2h/attackLog",
    name: "end",
    prams: {},
    component: endGame,
  },
]

const router = new VueRouter({
  routes,
  mode: "history"
});

new Vue({
  render: h => h(App),
  router
}).$mount("#app");
